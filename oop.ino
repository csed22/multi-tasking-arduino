// http://www.arduino.cc/en/Tutorial/Debounce
// https://learn.adafruit.com/multi-tasking-the-arduino-part-1/a-classy-solution

class Flasher
{
    // Class Member Variables
    // These are initialized at startup
    int ledPin;  // the number of the LED pin
    long OnTime; // milliseconds of on-time

    // These maintain the current state
    int ledState;                 // ledState used to set the LED
    unsigned long previousMillis; // will store last time LED was updated

    // Constructor - creates a Flasher
    // and initializes the member variables and state
public:
    Flasher(int pin, long on)
    {
        ledPin = pin;
        pinMode(ledPin, OUTPUT);

        OnTime = on;

        ledState = LOW;
        previousMillis = 0;
    }

    void Update()
    {
        // check to see if it's time to change the state of the LED
        unsigned long currentMillis = millis();

        if ((ledState == HIGH) && (currentMillis - previousMillis >= OnTime))
        {
            ledState = LOW;                 // Turn it off
            previousMillis = currentMillis; // Remember the time
            digitalWrite(ledPin, ledState); // Update the actual LED
        }
        else if ((ledState == LOW) && (currentMillis - previousMillis >= OnTime))
        {
            ledState = HIGH;                // turn it on
            previousMillis = currentMillis; // Remember the time
            digitalWrite(ledPin, ledState); // Update the actual LED
        }
    }

    void reduceOnTime(int reduction)
    {
        if (OnTime)
        {
            OnTime -= reduction;
            Serial.print("LED @");
            Serial.print(ledPin);
            Serial.print(" toggle state time is now ");
            Serial.print(OnTime);
            Serial.println("ms.");
        }
    }
};

class Button
{
    int buttonPin; // the number of the pushbutton pin

    // Variables will change:
    int buttonState = LOW;     // the current reading from the input pin
    int lastButtonState = LOW; // the previous reading from the input pin

    // the following variables are unsigned long's because the time, measured in miliseconds,
    // will quickly become a bigger number than can be stored in an int.
    unsigned long lastDebounceTime = 0; // the last time the output pin was toggled
    unsigned long debounceDelay = 10;   // the debounce time; increase if the output flickers

public:
    Button(int pin)
    {
        buttonPin = pin;
        pinMode(pin, INPUT);
    }

    // state changed
    int checkButtonPressed()
    {
        int reading = digitalRead(buttonPin);
        // check to see if you just pressed the button
        // (i.e. the input went from LOW to HIGH),  and you've waited
        // long enough since the last press to ignore any noise:

        // If the switch changed, due to noise or pressing:
        if (reading != lastButtonState)
        {
            // reset the debouncing timer
            lastDebounceTime = millis();
        }

        if ((millis() - lastDebounceTime) > debounceDelay)
        {
            // whatever the reading is at, it's been there for longer
            // than the debounce delay, so take it as the actual current state:

            // if the button state has changed:
            if (reading != buttonState)
            {
                buttonState = reading;

                // only toggle the LED if the new button state is HIGH
                if (buttonState == HIGH)
                    return true;
            }
        }

        // save the reading. Next time through the loop, it'll be the lastButtonState:
        lastButtonState = reading;
        return false;
    }
};

Flasher led1(8, 1000);
Flasher led2(12, 1000);

Button button1(3);
Button button2(5);

void setup()
{
    Serial.begin(9600);
    pinMode(13, OUTPUT);
}

void loop()
{
    if (button1.checkButtonPressed())
    {
        led1.reduceOnTime(100);
    }
    if (button2.checkButtonPressed())
    {
        led2.reduceOnTime(100);
    }
    led1.Update();
    led2.Update();
}