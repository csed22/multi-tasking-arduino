volatile int pin = 0;

//Inspired by: https://learn.adafruit.com/multi-tasking-the-arduino-part-1/a-classy-solution
class Flasher { 

    int ledPin; //Pin of the led
    int buttonPin; //Pin of the button
    long rate; //Rate of blinking
    int ledState; //Current state of the led on or off
    unsigned long prevMillis; //Previous time for blinking rate

public:
    Flasher(int pin1, int pin2)
    { //Constuctor
        ledPin = pin1;
        pinMode(ledPin, OUTPUT);
        buttonPin = pin2;
        pinMode(buttonPin, INPUT);
        rate = 1000;
        ledState = LOW;
        prevMillis = 0;
    }

    void ledUpdate()
    { //Update the state of the led on or off depending on the rate
        unsigned long currMillis = millis(); //current time
        if ((ledState == HIGH) && (currMillis - prevMillis >= rate)) {
            ledState = LOW;
            prevMillis = currMillis;
            digitalWrite(ledPin, ledState);
        }
        else if ((ledState == LOW) && (currMillis - prevMillis >= rate)) {
            ledState = HIGH;
            prevMillis = currMillis;
            digitalWrite(ledPin, ledState);
        }
    }

    void rateUpdate()
    { //Check if the button pin # equals that set by ISR
        if (buttonPin == pin) {
            //If equal reset the interrupt by setting the global variable to an arbitrary value say 0
            pin = 0;
            //Now proceed to modify the period (if the delay has expired) as before
            Decrease();
          
          	//DEBUG
            Serial.print("LED@");
            Serial.print(ledPin);
            Serial.print(": ");
            Serial.println(rate);
        }
    }

    void Decrease()
    { //Decrease the rate by 100 if the button is pushed but min is zero
        if (rate >= 100) {
            rate -= 100;
        }
        else if (rate > 0) {
            rate = 0;
        }
    }
};

//Intialize the 2 LEDs with the corrosponding push button
Flasher led1(12, 2); 
Flasher led2(8, 3);

void setup()
{
    //The ISR has only to identify which button is pressed
    //Use FALLING in ISR to detect the button press
    attachInterrupt(digitalPinToInterrupt(2), buttonPressed1, FALLING);
    attachInterrupt(digitalPinToInterrupt(3), buttonPressed2, FALLING);
  	//DEBUG  
 	Serial.begin(9600);
}

void loop()
{
    led1.rateUpdate();
    led2.rateUpdate();
    led1.ledUpdate();
    led2.ledUpdate();
}

//The ISR function must be declared outside the class definition

void buttonPressed1()
{
    pin = 2;
}

void buttonPressed2()
{
    pin = 3;
}